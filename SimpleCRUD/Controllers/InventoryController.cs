﻿using System;
using System.Threading;
using System.Threading.Tasks;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SimpleCRUD.Models;

namespace SimpleCRUD.Controllers
{
    public class InventoryController : Controller
    {
        InventoryViewModel item = new InventoryViewModel();
        // GET: Inventory
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        [AllowAnonymous]
        public ActionResult CommitInventory(InventoryItem objModel)
        {
            InventoryItem item = new InventoryItem();
            DataAccess da = new DataAccess();
            string strRet = string.Empty;

            strRet = da.udfSaveData(objModel);

            JsonResult result = new JsonResult();

            return View();
        }
    }
}