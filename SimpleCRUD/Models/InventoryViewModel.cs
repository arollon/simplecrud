﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.Sql;
using System.Data.SqlClient;
using System.Configuration;

namespace SimpleCRUD.Models
{
    public class InventoryViewModel
    {
        
    }

    public class InventoryItem
    {
        public int ItemID { get; set; }
        public string Desc { get; set; }
        public long Quantity { get; set; }
    }

    public class DataAccess
    {

        public string udfSaveData(InventoryItem objData)
        {
            DataSet dsCustomerInfo = new DataSet();
            SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ToString());

            using(System.Data.SqlClient.SqlCommand sqlcommands = new System.Data.SqlClient.SqlCommand("sprSaveItem", con))
            {
                sqlcommands.CommandType = CommandType.StoredProcedure;
                sqlcommands.Parameters.AddWithValue("@Description", objData.Desc);
                sqlcommands.Parameters.AddWithValue("@Quantity", objData.Quantity);
                sqlcommands.CommandTimeout = Convert.ToInt16(ConfigurationManager.AppSettings["SQLTimeoutParam"].ToString());
                SqlDataAdapter da = new SqlDataAdapter();
                da.SelectCommand = sqlcommands;
                dsCustomerInfo = new DataSet();
                da.Fill(dsCustomerInfo);
            }

            return "ok";
        }
    }
}